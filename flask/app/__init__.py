"""
Author:			Dhananjay Bhaskar
Description:	Main Python script for Dhananjay's portfolio website
Date Created:	03/04/2022
"""

from flask import Flask

#declare Flask instance
app = Flask(__name__)

from app import views