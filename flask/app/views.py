from app import app
from flask import render_template, redirect, url_for

#home page
@app.route("/")
def home():
	return render_template("home.html")

#about page
@app.route("/about")
def about():
	return render_template("about.html")


#blog page
@app.route("/blog")
def blog():
	return redirect('http://blog.djbhaskar.com')
	#return render_template("blog.html")


#blog post
@app.route("/blog_post")
def blog_post():
	return render_template("blog_post.html")